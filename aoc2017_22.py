

input = """....#.#...##..######.#...
.#####.#.#..#..#.##.#.##.
.#...#####.##....#.###.##
.##.##.#.####.####.##.###
#.#.##.#....###.#..####.#
..###...##..##...#.##.##.
.......##.###.###.###.###
#.#.#.#.....#.#..####.#..
##...#..#.#..##..#.#.####
#..#.....##.###.....#.###
.#.##..##.#####..##.###..
##....###....#.#.#.####.#
###.#.#..#.#.#.###....##.
##.##...#.##.#.##...#....
##....#.#.#.###...###.###
..##.###.#.####...###..#.
..##...##...#..##.#.#####
.##...##..#.#.#..##...#..
#.#.#.#..#####..####..###
..###.#.#....#.####..#..#
....#.#.###...#..#..#.#.#
###.##..###..##...##..#.#
...#.##..#.##.###......#.
.#..##.#####.#.##.....#.#
#.....##..####.##.###..#."""


lines = input.splitlines()
print(len(lines), len(lines[0]))



N = 10001
#N = 27
center = (N + 1) / 2
ll = len(lines)
halfStart = (ll+1) / 2

print("center=", center, ll, halfStart)

matrix = list()
defStr = '.' * N
defHStr = '.' * ((N + 1) / 2 - halfStart)
for i in range(center-halfStart):
    matrix.append(defStr)
for line in lines:
    newLine = defHStr + line + defHStr
    matrix.append(newLine)

for i in range(center-halfStart):
    matrix.append(defStr)

print("starting")
#print(lines)
#print('\n'.join(matrix))

dir = 0
x=center-1
y=center-1

def replaceStr(str, i, c):
    return str[:i] + c + str[i+1:]
    l = list(str)
    l[i] = c
    return ''.join(l)

def iter(x,y,d):
    #print(x, y)
    curr = matrix[x][y]
    i = 0
    if curr == '.':
        d = d - 1 if d != 0 else 3
        matrix[x] = replaceStr(matrix[x], y, 'W')
    elif curr == 'W':
        matrix[x] = replaceStr(matrix[x], y, '#')
        i = 1
    elif curr == 'F':
        matrix[x] = replaceStr(matrix[x], y, '.')
        d = d-2 if d >= 2 else d+2
    else:
        d = d + 1 if d != 3 else 0
        matrix[x] = replaceStr(matrix[x], y, 'F')

    if d == 0:
        x -= 1
    elif d == 1:
        y += 1
    elif d == 2:
        x += 1
    elif d == 3:
        y -= 1

    return (x,y,d, i)

infections = 0
m = 50
for i in range(10000000):
    inf = 0
    x,y,dir,inf = iter(x,y,dir)
    #print(x,y,dir,inf)
    #print('\n'.join(matrix))
    infections += inf
    if m < abs(center - x) or m < (center - y):
        m = max(abs(center - x),abs(center - y))
        print("max", m)
    if i%500000 == 0:
        print(i)

print(infections) # 9984  5263
