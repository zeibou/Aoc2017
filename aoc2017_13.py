import itertools
import heapq
import collections
import re

input = """0: 3
1: 2
2: 4
4: 4
6: 5
8: 8
10: 6
12: 6
14: 8
16: 6
18: 6
20: 8
22: 12
24: 8
26: 8
28: 12
30: 8
32: 12
34: 9
36: 14
38: 12
40: 12
42: 12
44: 14
46: 14
48: 10
50: 14
52: 12
54: 14
56: 12
58: 17
60: 10
64: 14
66: 14
68: 12
70: 12
72: 18
74: 14
78: 14
82: 14
84: 24
86: 14
94: 14"""

lines = input.splitlines()
layers = [(int(l[0]), int(l[1])) for l in [ll.split(": ") for ll in lines]]

print(layers)

dico = {l[0]: [l[1], 0, 1] for l in layers}
print(dico)

caught = []

for pos in range(95):
    if pos in dico.keys():
        curr = dico[pos][1]
        if curr == 0:
            caught.append(pos * dico[pos][0])
    for l in range(95):
        if l in dico.keys():
            curr = dico[l][1]
            if curr == 0:
                dico[l][1:2] = [1, 1]
            elif curr == dico[l][0] - 1:
                dico[l][1:2] = [dico[l][0] - 2, -1]
            else:
                dico[l][1] += dico[l][2]

print(caught)
print(sum(caught))

dico = {l[0]: [l[1], 0, 1] for l in layers}
print(dico)

def reduceW(depth, wait):
    return wait % ((depth - 1) * 2)


caught = [10]
wait = 0
while len(caught) > 0:
    caught = []
    wait += 1
    #print(wait)
    dico = {l[0]: [l[1], 0, 1] for l in layers}
    #print(dico)
    for l in range(95):
        if l in dico.keys():
            depth = dico[l][0]
            red = reduceW(depth, wait)
            '''if red == 0:
                pass
            elif red == depth - 1:
                dico[l][1:] = [dico[l][0] - 2, -1]
            if red <= (depth - 1):'''
            for ww in range(red):
                curr = dico[l][1]
                if curr == 0:
                    dico[l][1:] = [1, 1]
                elif curr == dico[l][0] - 1:
                    dico[l][1:] = [dico[l][0] - 2, -1]
                else:
                    dico[l][1] += dico[l][2]

    #print(wait, dico)
    for pos in range(95):
        #print("   ", pos, dico)
        if pos in dico.keys():
            curr = dico[pos][1]
            if curr == 0:
                caught.append(pos * dico[pos][0])
                break
        for l in range(95):
            if l in dico.keys():
                curr = dico[l][1]
                if curr == 0:
                    dico[l][1:] = [1, 1]
                elif curr == dico[l][0] - 1:
                    dico[l][1:] = [dico[l][0] - 2, -1]
                else:
                    dico[l][1] += dico[l][2]


print(wait) #22536
print(caught)
print(sum(caught))