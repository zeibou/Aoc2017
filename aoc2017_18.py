import itertools
import heapq
import collections
import re

input = """set i 31
set a 1
mul p 17
jgz p p
mul a 2
add i -1
jgz i -2
add a -1
set i 127
set p 464
mul p 8505
mod p a
mul p 129749
add p 12345
mod p a
set b p
mod b 10000
snd b
add i -1
jgz i -9
jgz a 3
rcv b
jgz b -1
set f 0
set i 126
rcv a
rcv b
set p a
mul p -1
add p b
jgz p 4
snd a
set a b
jgz 1 3
snd b
set f 1
add i -1
jgz i -11
snd a
jgz f -16
jgz a -19"""

cmds = [i for i in input.splitlines()]
print(cmds)

registers = []
for i in range(26):
    registers.append(0)
print(registers)

print(ord('c')-97)

def getVal(str, reg):
    return int(str) if str.isnumeric() or str[0] == '-' else reg[ord(str)-97]

i = 0
sound = 0
while 1:
    if i >= len(cmds):
        break
    cmd = cmds[i]
    spl = cmd.split(' ')
    if spl[0] == 'snd':
        sound = getVal(spl[1], registers)
    elif spl[0] == 'set':
        registers[ord(spl[1])-97] = getVal(spl[2], registers)
    elif spl[0] == 'add':
        registers[ord(spl[1])-97] += getVal(spl[2], registers)
    elif spl[0] == 'mul':
        registers[ord(spl[1])-97] *= getVal(spl[2], registers)
    elif spl[0] == 'mod':
        registers[ord(spl[1])-97] = registers[ord(spl[1])-97] % getVal(spl[2], registers)
    elif spl[0] == 'rcv':
        if getVal(spl[1], registers) != 0:
            print("recover", sound)
            break
    elif spl[0] == 'jgz':
        if getVal(spl[1], registers) != 0:
            i += getVal(spl[2], registers) - 1
    i += 1

print("done")



pr0 = []
pr1 = []
for i in range(26):
    pr0.append(0)
    pr1.append(0)

from collections import deque

d0 = deque()
d1 = deque()

sn = []
sn.append(0)

br = [0, 0]

def run(i, reg, deqS, deqR, p):
   # print(i)
    if i >= len(cmds):
        return -1
    cmd = cmds[i]
    spl = cmd.split(' ')
    if spl[0] == 'snd':
        deqS.append(getVal(spl[1], reg))
        if p:
            sn[0] += 1
        #print(p, "sends",getVal(spl[1], reg))
    elif spl[0] == 'set':
        reg[ord(spl[1])-97] = getVal(spl[2], reg)
    elif spl[0] == 'add':
        reg[ord(spl[1])-97] += getVal(spl[2], reg)
    elif spl[0] == 'mul':
        reg[ord(spl[1])-97] *= getVal(spl[2], reg)
    elif spl[0] == 'mod':
        reg[ord(spl[1])-97] = reg[ord(spl[1])-97] % getVal(spl[2], reg)
    elif spl[0] == 'rcv':
        if len(deqR) > 0:
            br[p] = 0
            ppp = deqR.popleft()
            #print(p, "receives", ppp)
            reg[ord(spl[1]) - 97] = ppp
                #print("count", sn[0])
        else:
            br[p] = 1
            i -= 1
    elif spl[0] == 'jgz':
        if getVal(spl[1], reg) > 0:
            i += getVal(spl[2], reg) - 1
    i += 1
    return i

i = 0
j = 0
pr1[ord('p') - 97] = 1
k0 = 0
k1 = 0
while 1:
    if i >= 0:
        i = run(i, pr0, d0, d1, 0)
    else :
        k0 = 1
    if j >= 0:
        j = run(j, pr1, d1, d0, 1)
    else:
        k1 = 1
    if br[0] and br[1] or k1 and k0:
        break
print("done")
print("count", sn[0])