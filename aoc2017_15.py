import itertools
import heapq
import collections
import re

input = ""
genAFactor = 16807
genBFactor = 48271
divisor = 2147483647


genAPrev = 873
genBPrev = 583

def Next(prev, fact):
    a = prev * fact
    r = a % divisor
    return r

def Bin16(i):
    b = bin(i).split('b')[-1].zfill(32)
    return b[16:]
'''
c = 0
for i in range(40000000):
    genAPrev = Next(genAPrev, genAFactor)
    genBPrev = Next(genBPrev, genBFactor)
    ba = Bin16(genAPrev)
    bb = Bin16(genBPrev)
    #print(ba)
    #print(bb)
    if ba == bb:
        c+= 1

print(c) #228
'''

def Next2(prev, fact, m):
    r = 1
    while r % m != 0 :
        a = prev * fact
        r = a % divisor
        prev = r
    return r
c = 0
for i in range(5000000):
    genAPrev = Next2(genAPrev, genAFactor, 4)
    genBPrev = Next2(genBPrev, genBFactor, 8)
    ba = Bin16(genAPrev)
    bb = Bin16(genBPrev)
    #print(ba)
    #print(bb)
    if ba == bb:
        c+= 1

print(c) #228
