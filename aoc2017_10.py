import itertools
import collections

input = """130,126,1,11,140,2,255,207,18,254,246,164,29,104,0,224"""

#input = '3,4,1,5'
lengths = input.split(',')
arr = range(0, 256)
print(arr)

#arr = range(0,5)
max = len(arr)



def work(n):
    pos = 0
    skip = 0
    for i in range(n):
        for ls in lengths:
            l = int(ls)
            #print ("length:", l)
            if pos + l < max:
                r = arr[pos:pos+l]
                r.reverse()
                arr[pos:pos + l] = r
            else:
                r = arr[pos:]+arr[:pos+l-max]
                r.reverse()
                #print("r", r, pos, r[pos+l-max:], r[:-pos+1])
                arr[pos:] = r[:max-pos]
                arr[:pos+l-max] = r[max-pos:]
            pos = (pos+l+skip)%max
            skip += 1

work(1)
print(arr[0] * arr[1])

input='AoC 2017'
#33efeb34ea91902bb2f59c9920caa6cd
#33efeb34ea91902bb2f59c9920caa6cd

addL = (17, 31, 73, 47, 23)
#lengths = [int(i)+48 if i !=',' else 44 for i in input]

input = "130,126,1,11,140,2,255,207,18,254,246,164,29,104,0,224"
lengths = [ord(i) for i in input]
lengths += addL
print(lengths)
arr = range(0, 256)
work(64)
print(arr)



def densify(l):
    r = reduce(lambda x,y : x ^ y, l)
    return r


hash = ""
for i in range(16):
    sl = arr[16*i:16*(i+1)]
    d = densify(sl)
    print(d, hex(d))
    hash += hex(d).split('x')[-1].zfill(2)

print(hash)

#print(densify((65 , 27 , 9 , 1 , 4 , 3 , 40 , 50 , 91 , 7 , 6 , 0 , 2 , 5 , 68 , 22)))

#e1462100a34221a7f0906da15c1c979a
#