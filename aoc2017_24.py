import itertools
import heapq
import collections
import re

input = '''14/42
2/3
6/44
4/10
23/49
35/39
46/46
5/29
13/20
33/9
24/50
0/30
9/10
41/44
35/50
44/50
5/11
21/24
7/39
46/31
38/38
22/26
8/9
16/4
23/39
26/5
40/40
29/29
5/20
3/32
42/11
16/14
27/49
36/20
18/39
49/41
16/6
24/46
44/48
36/4
6/6
13/6
42/12
29/41
39/39
9/3
30/2
25/20
15/6
15/23
28/40
8/7
26/23
48/10
28/28
2/13
48/14'''


lines  = input.splitlines()
components = list()
for l in lines:
    components.append(list(map(int, l.split('/'))))


print(components)

def CreateDico():
    dico = dict()

m = []
m.append(0)
m.append(0)
m.append(0)

def checkNext(curr, avail, total, length):
    total += curr
    for a in avail:
        if a[0] == curr:
            newAv = [i for i in avail if i != a]
            checkNext(a[1], newAv, total + curr, length + 1)
        elif a[1] == curr:
            newAv = [i for i in avail if i != a]
            checkNext(a[0], newAv, total + curr, length + 1)
    else:
        m[0] = max(m[0], total)
        if m[2] < length:
            m[1] = total
            m[2] = length
        elif m[2] == length:
            m[1] = max(m[1], total)


checkNext(0, components, 0, 0)
print(m)