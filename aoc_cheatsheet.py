import itertools
import heapq
import collections
import re

input = """aaa"""
lines = input.splitlines()
items = input.split("-")


a = ord("a")
b = chr(97)
print(a,b)

h= hex(254)
hs = h.split('x')[-1]
print(h, hs)

hs3 = hs.zfill(3)
print(hs3)

print("ljust", hs.ljust(5,"-"))
print("rjust", hs.rjust(5,"+"))
print("center", hs.center(5,"*"))

