input = """Begin in state A.
Perform a diagnostic checksum after 12629077 steps.

In state A:
  If the current value is 0:
    - Write the value 1.
    - Move one slot to the right.
    - Continue with state B.
  If the current value is 1:
    - Write the value 0.
    - Move one slot to the left.
    - Continue with state B.

In state B:
  If the current value is 0:
    - Write the value 0.
    - Move one slot to the right.
    - Continue with state C.
  If the current value is 1:
    - Write the value 1.
    - Move one slot to the left.
    - Continue with state B.

In state C:
  If the current value is 0:
    - Write the value 1.
    - Move one slot to the right.
    - Continue with state D.
  If the current value is 1:
    - Write the value 0.
    - Move one slot to the left.
    - Continue with state A.

In state D:
  If the current value is 0:
    - Write the value 1.
    - Move one slot to the left.
    - Continue with state E.
  If the current value is 1:
    - Write the value 1.
    - Move one slot to the left.
    - Continue with state F.

In state E:
  If the current value is 0:
    - Write the value 1.
    - Move one slot to the left.
    - Continue with state A.
  If the current value is 1:
    - Write the value 0.
    - Move one slot to the left.
    - Continue with state D.

In state F:
  If the current value is 0:
    - Write the value 1.
    - Move one slot to the right.
    - Continue with state A.
  If the current value is 1:
    - Write the value 1.
    - Move one slot to the left.
    - Continue with state E."""

input = input.replace("A:", "0:")
input = input.replace("B:", "1:")
input = input.replace("C:", "2:")
input = input.replace("D:", "3:")
input = input.replace("E:", "4:")
input = input.replace("F:", "5:")
input = input.replace("A.", "0.")
input = input.replace("B.", "1.")
input = input.replace("C.", "2.")
input = input.replace("D.", "3.")
input = input.replace("E.", "4.")
input = input.replace("F.", "5.")
input = input.replace("left", "0")
input = input.replace("right", "2")
lines = input.splitlines()

startState = int(lines[0][-2])
checksumStep = int(lines[1].split(" ")[-2])
states = list()
for s in range(3, len(lines), 10):
    states.append(((int(lines[s + 2][-2]), int(lines[s + 3][-2]), int(lines[s + 4][-2])),
                   (int(lines[s + 6][-2]), int(lines[s + 7][-2]), int(lines[s + 8][-2]))))


print(states)

i = 0
positives = []
negatives = [0]

def step(i, s):
    state = states[s]
    if i >= 0:
        if i >= len(positives):
            positives.append(0)
        curr = positives[i]
        positives[i] = state[curr][0]
    else:
        if -i >= len(negatives):
            negatives.append(0)
        curr = negatives[-i]
        negatives[-i] = state[curr][0]
    i += state[curr][1] - 1
    next = state[curr][2]
    return (i, next)

i = 0
s = startState
for n in range(checksumStep):
    i, s = step(i, s)

print("sum", sum(positives) + sum(negatives))