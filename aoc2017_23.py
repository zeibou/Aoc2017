import itertools
import heapq
import collections
import re

inputhelp = """
set b 84
set c b
jnz a 2    # part 2 don't skip following
jnz 1 5    # part1 skip following
mul b 100
sub b -100000
set c b
sub c -17000

    set f 1     # start  b=108400, c= b+17000 = 125400    jnz 1 -23
    set d 2     # 
        set e 2     # jnz g -13
            set g d     # jnz g -8
            mul g e
            sub g b
            jnz g 2  # set f to 0 if g==0
                set f 0 
            sub e -1   # e++
            set g e
            sub g b
            jnz g -8
        sub d -1
        set g d
        sub g b
        jnz g -13
    jnz f 2  # h++ if f==0
        sub h -1  
    set g b
    sub g c
    jnz g 2
    jnz 1 3   #exit
    sub b -17   # b+= 17
    jnz 1 -23"""

def runOptim():
    a,b,c,d,e,f,g,h = 1, 0, 0, 2, 1, 1, 1, 0
    mul = 0
    b = 84
    c = b
    if a:
        b *= 100
        b += 100000
        c = b + 17000
    while 1:
        print("loop 0 ", a,b,c,d,e,f,g,h)
        f = 1
        for d in range(2 - b, 0):
            if d%100 == 0:
                print("loop 1 ", a,b,c,d,e,f,g,h)
            for e in range(2 - b, 0):
                #print("loop 2 ", a,b,c,d,e,f,g,h)
                g = d
                g *= e
                mul += 1
                g -= b
                if g == 0:
                    f = 0
        if f == 0:
            h += 1
        g = b
        g -= c
        if g != 0:
            b += 17
        else:
            print(a,b,c,d,e,f,g,h, mul)
            break


#runOptim()  # 999 too high, 600 too low, 998
#exit()

part2 = 1
def runOptim2():
    a,b,c,d,e,f,g,h = part2, 0, 0, 0, 0, 0, 0, 0
    mul = 0
    b = 84
    c = b
    if a:
        b *= 100
        b += 100000
        c = b + 17000
    while 1:
        print("loop 0 ", a,b,c,d,e,f,g,h)
        f = 1
        d = 2
        while 1:
            e = 2
            if b % d == 0 and int(b / d) >= 2:
                f = 0
            d += 1
            g = d
            g -= b
            if g == 0:
                break
        if f == 0:
            h += 1
        g = b
        g -= c
        if g != 0:
            b += 17
        else:
            print(a,b,c,d,e,f,g,h, mul)
            break


def runOptimKeep():
    a,b,c,d,e,f,g,h = part2, 0, 0, 0, 0, 0, 0, 0
    mul = 0
    b = 84
    c = b
    if a:
        b *= 100
        b += 100000
        c = b + 17000
    while 1:
        print("loop 0 ", a,b,c,d,e,f,g,h)
        f = 1
        d = 2
        while 1:
            e = 2
            while 1:
                g = d
                g *= e
                mul += 1
                g -= b
                if g == 0:
                    f = 0
                e += 1
                g = e
                g -= b
                if g == 0:
                    break
            d += 1
            g = d
            g -= b
            if g == 0:
                break
        if f == 0:
            h += 1
        g = b
        g -= c
        if g != 0:
            b += 17
        else:
            print(a,b,c,d,e,f,g,h, mul)
            break


runOptim2()  # 999 too high, 600 too low, 998
exit()

input = """set b 84
set c b
jnz a 2    # part 2 don't skip following
jnz 1 5    # part1 skip following
mul b 100
sub b -100000
set c b
sub c -17000
set f 1     # start  b=108400, c= b+17000    jnz 1 -23
set d 2     # 
set e 2     # jnz g -13
set g d     # jnz g -8
mul g e
sub g b
jnz g 2
set f 0
sub e -1
set g e
sub g b
jnz g -8
sub d -1
set g d
sub g b
jnz g -13
jnz f 2
sub h -1
set g b
sub g c
jnz g 2
jnz 1 3   #exit
sub b -17
jnz 1 -23"""

cmds = [i for i in input.splitlines()]
print(cmds)


print(ord('c')-97)

def isnumeric(str):
    #import re
    #num_format = re.compile("^[\-]?[1-9]*[0-9]*\.?[0-9]*$")
    #return re.match(num_format, str)
    #print(str, str[0], ord(str[0]), ord('0'), ord('9'))

    return str[0] == '-' or ord(str[0]) >= ord('0') and ord(str[0]) <= ord('9')

def getVal(str, reg):
    return int(str) if isnumeric(str) else reg[ord(str)-97]



pr0 = []
for i in range(8):
    pr0.append(0)

pr0[0] = 1

def run(i, reg):
    m = 0
    if i >= len(cmds):
        return -1, 0
    cmd = cmds[i]
    spl = cmd.split(' ')
    #print(i, cmd, reg)
    if spl[0] == 'set':
        reg[ord(spl[1])-97] = getVal(spl[2], reg)
    elif spl[0] == 'sub':
        reg[ord(spl[1])-97] -= getVal(spl[2], reg)
    elif spl[0] == 'mul':
        reg[ord(spl[1])-97] *= getVal(spl[2], reg)
        m = 1
    elif spl[0] == 'jnz':
        #print(spl)
        if getVal(spl[1], reg) != 0:
            i += getVal(spl[2], reg) - 1
    i += 1
    return i, m

i = 0
mul = 0
j = 0
while i >= 0:
    j+=1
    cmd = cmds[i]
    if j%100000 == 0:
        print(i, cmd, pr0)
    i, m = run(i, pr0)
    mul += m
    #if j > 100:
    #    break

print("done")
print("count", mul)
print("h=", pr0[7])